/*global leojpod, $, document, windows, THREE*/
'use strict';
(function () {
  function EventLimiter(time) {
    var timer = null;
    this.isFreeToWork = function () {
      if (timer !== null) {
        //no...
        return false;
      } else {
        timer = setTimeout(function() {
          timer = null;
          console.log('free to work');
        }, time);
        return true;
      }
    };
  }
  function updateLevels(orientation, visual) {
    $('#rel-roll').val(orientation.relativeRoll);
    $('#rel-pitch').val(orientation.relativePitch);
    $('#roll').val(orientation.roll);
    $('#pitch').val(orientation.pitch);
    $('#yaw').val(orientation.yaw);
    $('#compass').val(orientation.compass);
    visual.relRoll.push(orientation.relativeRoll);
    visual.relPitch.push(orientation.relativePitch);
    visual.pitch.push(orientation.pitch);
    visual.roll.push(orientation.roll);
    visual.yaw.push(orientation.yaw);
    visual.compass.push(orientation.compass);
    // console.log('levels updated');
  }

  function initPage() {
    var lib, tD, tabletBox, monitor,// refreshLimiter = new EventLimiter(50),
      handlingOver = true,
      visual = {};
    console.log('checking for required libraries');
    if (typeof leojpod === 'undefined') {
      console.error('leojpod is not defined');
      throw 'leojpod modules required!';
    }
    else {
      lib = leojpod.orientation;
      tD = leojpod.threeD;
    }
    console.log('initialising objects');


    //resize #tablet
    console.log('resizing #tablet');
    var height = $('#tablet-3d-view').height();
    // console.log('height : ' + height);
    // var headerHeight = $('[data-role="header"]').outerHeight();
    // var levelsHeight = $('#levels').outerHeight();
    // console.log('heights : ' + headerHeight + '/' + levelsHeight);
    $('#tablet-3d-view').height($('body').height() - 10 - $('[data-role="header"]').outerHeight() - $('#levels').outerHeight());
    height = $('#tablet-3d-view').height();
    // console.log('height2 : ' + height);
    $('#tablet').height($('#tablet-3d-view').height());
    height = $('#tablet').height();
    // console.log('height3 : ' + height);

    console.log('preparing a timeout of 0 just to let background tasks time to complete');
    setTimeout(function () {
      console.log('creating the box');
      //tablet box
      tabletBox = new tD.BoxMapper();
      visual.relRoll = new lib.SimpleAngleVisualisor($('#rel-roll-canvas'));
      visual.relPitch = new lib.SimpleAngleVisualisor($('#rel-pitch-canvas'));
      visual.roll = new lib.SimpleAngleVisualisor($('#roll-canvas'));
      visual.pitch = new lib.SimpleAngleVisualisor($('#pitch-canvas'));
      visual.yaw = new lib.SimpleAngleVisualisor($('#yaw-canvas'));
      visual.compass = new lib.SimpleAngleVisualisor($('#compass-canvas'));
      // orientation = new lib.OrientationMonitor();
      console.log('adding handler on the box buttons');
      $('button.face-button').click(function () {
        console.log('click on: ' + $(this).data('place'));
      });
      tabletBox.init($('#tablet'), {
        x: 400,
        y: 480,
        z: 100
      });
      tabletBox.cameraPosition(0, 0, 1000);
      console.log('animating the box');
      tabletBox.animate();
      lib.testOrientationAbility(function (yes) {
        console.log('now we know... : ' + yes);
        if (yes) {
          $('#tablet-3d-view').after('<p>move your device and the model should move accordindly</p>');
          monitor = new lib.OrientationMonitor();
          monitor.triggerDelta = 1;
          monitor.precision = 0;
          $(window).on('orientationChangedEvent', function (event, orientation) {
            // console.log('orientation!');
            // if (handlingOver) {
            //   handlingOver = false;
              updateLevels(orientation, visual);
              tabletBox.setRotation(
                THREE.Math.degToRad(-orientation.relativePitch),
                THREE.Math.degToRad(-orientation.relativeRoll),
                THREE.Math.degToRad(-orientation.yaw));
            //   handlingOver = true;
            // } else {
            //   console.log('too frequent update!');
            // }
          });
          // $(window).on('rotationRateChangedEvent', function (event, orientation, interval) {
          //   tabletBox.rotate(
          //   THREE.Math.degToRad(-orientation.relativePitch * interval / 1000),
          //   THREE.Math.degToRad(-orientation.relativeRoll * interval / 1000),
          //   THREE.Math.degToRad(-orientation.yaw * interval / 1000));
          // });
          monitor.start_logging();

        }
        else {
          // setInterval(function () {
          //   tabletBox.rotate(Math.random() * 0.2,Math.random() * 0.2,Math.random() * 0.2);
          // }, 200);
          tabletBox.enableTrackball(true);
          $('#tablet-3d-view').after('<p>no motion sensor, so use the mouse to move the 3D model around</p>');
        }
      });
    }, 0);
    console.log('the timeout should take over now');
  }


  $(document).ready(function () {
    // console.log('0');
    // $.getScript('public/javascripts/tools/three.min.js', function () {
    //   console.log('1');
    //   $.getScript('public/javascripts/tools/CSS3DRenderer.js', function () {
    //     console.log('2');
    //     $.getScript('public/javascripts/boxMapper.js', function () {
    //       console.log('3');
    console.log('let\'s init the page');
    initPage();
    console.log('page_initialised!');
    //     });
    //   });
    // });
  });
})();
