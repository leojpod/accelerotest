/*global leojpod, $, document, windows, THREE*/
'use strict';
(function () {

  function initPage() {
    var lib, tDtools, tDworld;
    var world, worldControls;

    function initOrientationBrowsing(useCSS3D) {
      console.log('orientation support! ' + (useCSS3D? '(with CSS3D support)':'(w/o CSS3D support)'));
      initPointerBrowsing(useCSS3D);
    }
    function initPointerBrowsing(useCSS3D) {
      console.log('PointerBrowsing support! ' + (useCSS3D? '(with CSS3D support)':'(w/o CSS3D support)'));
      world = new tDworld.World3D($('#world3D'), useCSS3D);
      worldControls = new THREE.SimpleTrackballControls( world.getCamera(), world.getElement());
    }
    console.log('checking for required libraries');
    if (typeof leojpod === 'undefined') {
      console.error('leojpod is not defined');
      throw 'leojpod modules required!';
    }
    else {
      lib = leojpod.orientation;
      tDtools = leojpod.threeDTools;
      tDworld = leojpod.world;
    }

    console.log('preparing a timeout of 0 just to let background tasks time to complete');
    setTimeout(function () {
      var css3DSupport = tDtools.has3D();
      lib.testOrientationAbility(function (yes) {
        console.log('now we know... : ' + yes);
        if (yes) {
          initOrientationBrowsing(css3DSupport);
        }
        else {
          // initPointerBrowsing(css3DSupport);
          initPointerBrowsing(false);
        }

        world.setAnimate(true);

        //add button listners
        $('#zoom').click(function () { worldControls.stepZoom(true);});
        $('#unzoom').click(function () { worldControls.stepZoom(false);});
        $('#menu').click(function (){ world.movePlane(new THREE.Vector3(Math.random()*100, Math.random()*100, Math.random()*100))});
      });
    }, 0);
    console.log('the timeout should take over now');
  }


  $(document).ready(function () {
    // console.log('0');
    // $.getScript('public/javascripts/tools/three.min.js', function () {
    //   console.log('1');
    //   $.getScript('public/javascripts/tools/CSS3DRenderer.js', function () {
    //     console.log('2');
    //     $.getScript('public/javascripts/boxMapper.js', function () {
    //       console.log('3');
    console.log('let\'s init the page');
    initPage();
    console.log('page_initialised!');
    //     });
    //   });
    // });
  });
})();
