/*global THREE*/

THREE.CombinedCamera = function (camera1, camera2) {
  THREE.PerspectiveCamera.call(this);
  this.firstCam = camera1;
  this.secondCam = camera2;
  // this.handleSingleCamCase = function (fct1cam, fct2cam) {
  //   if (this.secondCam === 'undefined') {
  //     return fct1cam;
  //   } else {
  //     return fct2cam;
  //   }
  // };
};

THREE.CombinedCamera.prototype = Object.create(THREE.PerspectiveCamera.prototype);

THREE.CombinedCamera.prototype.handleSingleCamCase = function (fct1cam, fct2cam) {
  if (typeof this.secondCam === 'undefined') {
    return fct1cam;
  } else {
    return fct2cam;
  }
};

THREE.CombinedCamera.prototype.setLens = function () {
  var _this = this;
  console.log(this);
  return this.handleSingleCamCase(
    function (focalLength, frameSize) {
      _this.firstCam.setLens(focalLength, frameSize);
    },
    function (focalLength, frameSize) {
      _this.firstCam.setLens(focalLength, frameSize);
      _this.secondCam.setLens(focalLength, frameSize);
    });
}();

THREE.CombinedCamera.prototype.setViewOffset = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function ( fullWidth, fullHeight, x, y, width, height ) {
      _this.firstCam.setViewOffset(fullWidth, fullHeight, x, y, width, height);
    },
    function ( fullWidth, fullHeight, x, y, width, height ) {
      _this.firstCam.setViewOffset(fullWidth, fullHeight, x, y, width, height);
      _this.secondCam.setViewOffset(fullWidth, fullHeight, x, y, width, height);
    });
}();

THREE.CombinedCamera.prototype.updateProjectionMatrix = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function () {
      _this.firstCam.updateProjectionMatrix();
    },
    function () {
      _this.firstCam.updateProjectionMatrix();
      _this.secondCam.updateProjectionMatrix();
    });
}();

THREE.CombinedCamera.prototype.lookAt = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (vector) {
      _this.firstCam.lookAt(vector);
    },
    function (vector) {
      _this.firstCam.lookAt(vector);
      _this.secondCam.lookAt(vector);
    });
}();

THREE.CombinedCamera.prototype.setFov = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newFov) {
      _this.firstCam.fov = newFov;
    },
    function (newFov) {
      _this.firstCam.fov = newFov;
      _this.secondCam.fov = newFov;
    });
}();

THREE.CombinedCamera.prototype.setAspect = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newAspect) {
      _this.firstCam.aspect = newAspect;
    },
    function (newAspect) {
      _this.firstCam.aspect = newAspect;
      _this.secondCam.aspect = newAspect;
    });
}();

THREE.CombinedCamera.prototype.setNear = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newNear) {
      _this.firstCam.near = newNear;
    },
    function (newNear) {
      _this.firstCam.near = newNear;
      _this.secondCam.near = newNear;
    });
}();

THREE.CombinedCamera.prototype.setFar = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newFar) {
      _this.firstCam.far = newFar;
    },
    function (newFar) {
      _this.firstCam.far = newFar;
      _this.secondCam.far = newFar;
    });
}();

THREE.CombinedCamera.prototype.setMatrixWorldInverse = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newMatrix) {
      _this.firstCam.matrixWorldInverse = newMatrix;
    },
    function (newMatrix) {
      _this.firstCam.matrixWorldInverse = newMatrix;
      _this.secondCam.matrixWorldInverse = newMatrix;
    });
}();

THREE.CombinedCamera.prototype.setProjectionMatrix = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (newMatrix) {
      _this.firstCam.projectionMatrix = newMatrix;
    },
    function (newMatrix) {
      _this.firstCam.projectionMatrix = newMatrix;
      _this.secondCam.projectionMatrix = newMatrix;
    });
}();

THREE.CombinedCamera.prototype.setProjectionMatrixInverse = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (matrix) {
      _this.firstCam.projectionMatrixInverse = matrix;
    },
    function (matrix) {
      _this.firstCam.projectionMatrixInverse = matrix;
      _this.secondCam.projectionMatrixInverse = matrix;
    });
}();

THREE.CombinedCamera.prototype.setPosition = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (vector) {
      _this.firstCam.position = vector;
    },
    function (vector) {
      _this.firstCam.position = vector;
      _this.secondCam.position = vector;
    });
}();


THREE.CombinedCamera.prototype.setRotation = function () {
  var _this = this;
  return this.handleSingleCamCase(
    function (vector) {
      _this.firstCam.rotation = vector;
    },
    function (vector) {
      _this.firstCam.rotation = vector;
      _this.secondCam.rotation = vector;
    });
}();


THREE.CombinedCamera.prototype.getPosition = function () {
  return this.firstCam.position;
};

// THREE.CombinedCamera.prototype.set = function () {
//   var _this = this;
//   return this.handleSingleCamCase(
//     function () {
//       _this.firstCam.
//     },
//     function () {
//       _this.firstCam.
//       _this.secondCam.
//     });
// }();