/*global $, leojpod:true, THREE, requestAnimationFrame*/

'use strict';
if (typeof THREE === 'undefined') {
  throw 'three.js is requried for this module!!!';
}
if (typeof this.leojpod === 'undefined') {
  this.leojpod = {};
}
(function (exports) {
  exports.BoxMapper = function () {
    var camera, scene, renderer, box, divs = {}, controls;
    function animate() {
      requestAnimationFrame(animate);

      if (typeof controls !== 'undefined'  && controls.noRotate === false) {
        controls.update();
      }
      renderer.render(scene, camera);
    }
    this.animate = function () {
      animate();
    };

    this.init = function (divContainer, size) {
      console.log('BoxMapper init');
      var width, height, max;
      width = $(divContainer).width();
      height = $(divContainer).height();
      console.log('conatiner size: ' + width + '/' + height);
      console.log('windows.height: ' + $(window).height());
      camera = new THREE.PerspectiveCamera(45, width / height, 1, 1000);
      // camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 1000 );
			camera.position.set( 0, 0, 700 );

      scene = new THREE.Scene();
      // var element = $(divContainer).get(0);
      // console.log('divConatiner: ');
      // console.log(element);
      box = new THREE.CSS3DObject( $('<div>').get(0));
      //now look for every element of the box..
      // element = $(divContainer).children('.top').get(0);
      // console.log('.top: ')
      // console.log(element);
      divs.top = new THREE.CSS3DObject($(divContainer).children('.top').get(0), true, true);
      divs.bottom = new THREE.CSS3DObject($(divContainer).children('.bottom').get(0), true, true);
      divs.left = new THREE.CSS3DObject($(divContainer).children('.left').get(0), true, true);
      divs.right = new THREE.CSS3DObject($(divContainer).children('.right').get(0), true, true);
      divs.front = new THREE.CSS3DObject($(divContainer).children('.front').get(0), true, true);
      divs.back = new THREE.CSS3DObject($(divContainer).children('.back').get(0), true, true);
      //resize them!
      $(divContainer).children('.front, .back').width(size.x).height(size.y);
      $(divContainer).children('.top, .bottom').width(size.x).height(size.z);
      $(divContainer).children('.right, .left').width(size.y).height(size.z);
      //now place them according to the size...
      divs.front.position = new THREE.Vector3(0, 0, size.z / 2);
      divs.back.position = new THREE.Vector3(0, 0, - size.z / 2);
      divs.back.rotation = new THREE.Vector3(0, Math.PI, 0);

      divs.top.position = new THREE.Vector3(0, size.y / 2, 0);
      divs.top.rotation = new THREE.Vector3(- Math.PI / 2, 0, 0);
      divs.bottom.position = new THREE.Vector3(0, - size.y / 2, 0);
      divs.bottom.rotation = new THREE.Vector3(Math.PI / 2, 0, 0);


      divs.left.position = new THREE.Vector3(size.x / 2, 0, 0);
      divs.left.rotation = new THREE.Vector3(Math.PI / 2, Math.PI / 2, 0);
      divs.right.position = new THREE.Vector3(- size.x / 2, 0, 0);
      divs.right.rotation = new THREE.Vector3(- Math.PI / 2, - Math.PI / 2, 0);

      box.add(divs.top);
      box.add(divs.bottom);
      box.add(divs.front);
      box.add(divs.back);
      box.add(divs.left);
      box.add(divs.right);

      // box.position = new THREE.Vector3(100,500,-200);
      // scene.add(divs.top);
      // scene.add(divs.bottom);
      // scene.add(divs.front);
      // scene.add(divs.back);
      // scene.add(divs.left);
      // scene.add(divs.right);
      scene.add(box);
      console.log('scene created');
      // box.rotation = new THREE.Vector3(0,1,1);
      renderer = new THREE.CSS3DRenderer();
      renderer.render(scene, camera);
      renderer.setSize( width, height );
			$(divContainer).append( renderer.domElement );
      renderer.domElement.style.backgroundColor='#aaa';
      console.log('scene rendered');
    };

    this.rotate = function (x, y, z) {
      box.rotation.x += x;
      box.rotation.y += y;
      box.rotation.z += z;
    };
    this.setRotation = function (x, y, z) {
      box.rotation = new THREE.Vector3(x, y, z);
    };
    this.cameraPosition = function (x, y, z) {
      if (typeof x === 'undefined') {
        return camera.position;
      } else {
        camera.position = new THREE.Vector3(x, y, z);
      }
    };
    this.enableTrackball = function (enable) {
      if (typeof enable === 'undefined' || enable === true) {
        controls = new THREE.TrackballControls( camera, renderer.domElement );
        controls.rotateSpeed = 1.0;
        controls.zoomSpeed = 1.2;
				controls.panSpeed = 0.8;

				controls.noZoom = true;
				controls.noPan = true;

				controls.staticMoving = false;
				controls.dynamicDampingFactor = 0.3;
      } else {
        controls.noRotate = true;
      }
    };
  };
}) (this.leojpod.threeD = {});
console.log('BoxMapper loaded!')