/*global window, $, leojpod:true*/
'use strict';
if(typeof this.leojpod === 'undefined') {
  this.leojpod = {};
}
(function (exports) {
  Math.FromDegToRad = function (val) {
    return val * Math.PI / 180;
  };
  Math.FromRadToDeg = function (val) {
    return val * 180 / Math.PI;
  };
  exports.testOrientationAbility = function (callback) {
    var timeout;
    if(typeof callback !== 'function') {
      throw 'testOrientationAbility expect a function argument to catch the test result!';
    }
    timeout = setTimeout(function() {
      callback(false);
    }, 1000);
    function orientationTestMethod(event) {
      clearTimeout(timeout);
      $(window).off('deviceorientation', orientationTestMethod);
      if (typeof event.originalEvent.beta !== 'undefined' &&
          event.originalEvent.beta !== null &&
          typeof event.originalEvent.gamma !== 'undefined'){
            callback(true);
      } else {
        callback(false);
      }
    }
    $( window ).on('deviceorientation', orientationTestMethod);
  };
  exports.Orientation = function () {
    var _pitch = 0, _roll = 0, _yaw = 0, _relPitch = 0, _relRoll = 0, _compass = 0;
    this.__defineGetter__("pitch", function() {
      return _pitch;
    });
    this.__defineGetter__("roll", function() {
      return _roll;
    });
    this.__defineGetter__("relativePitch", function() {
      return _relPitch;
    });
    this.__defineGetter__("relativeRoll", function() {
      return _relRoll;
    });
    this.__defineGetter__("yaw", function() {
      return _yaw;
    });
    this.__defineGetter__("compass", function() {
      return _compass;
    });
    this.getData = function() {
      return {
        pitch: _pitch,
        roll: _roll,
        relativePitch: _relPitch,
        relativeRoll: _relRoll,
        yaw: _yaw,
        compass: _compass,
        // bubbles: false,
        // cancelable: true
      };
    };
    this.update =  function(newPitch, newRoll, newYaw, newCompass) {
      _pitch = newPitch % 180;
      if (_pitch > 90){
        _pitch = 0 - (180 - _pitch);
      } else if (_pitch < -90) {
        _pitch = 180 + _pitch;
      }
      _roll = newRoll % 180;
      if (_roll > 90){
        _roll = 0 - (180 - _roll);
      } else if (_roll < -90) {
        _roll = 180 + _roll;
      }
      _yaw = newYaw % 360;
      _compass = newCompass % 360;
      switch (window.orientation) {
      case 0:
        _relRoll = _roll;
        _relPitch = _pitch;
        break;
      case 180:
        _relRoll = _roll * -1;
        _relPitch = _pitch * -1;
        break;
      case 90:
        _relRoll = _pitch;
        _relPitch = _roll * -1;
        break;
      case -90:
        _relRoll = _pitch * -1;
        _relPitch = _roll;
        break;
      default:
        _relRoll = _roll;
        _relPitch = _pitch;
      }
    };
  };

  exports.SimpleAngleVisualisor = function (div) {
        var canvas, ctx, jDiv = $(div), currentAngle, width, height;
        function drawIndicator() {
          var halfH, fourthH;
          width = jDiv.width() ;
          height = jDiv.height() ;
          halfH = Math.floor(height/2) - 2;
          fourthH = Math.floor(height/4) - 1;
          // console.log('drawIndicator: ' + width + '/' + height + '/' + halfH + '/' + fourthH);
          ctx.setTransform(1, 0, 0, 1, 0, 0);
          ctx.clearRect(0, 0, width, height);
          ctx.translate(width / 2, halfH + 2);
          ctx.rotate(currentAngle);
          ctx.lineWidth = 2;
          ctx.lineCap = 'round';
          ctx.lineJoin = 'round';
          ctx.shadowOffsetX = 5;
          ctx.shadowOffsetY = 5;
          ctx.shadowBlur = 5;
          ctx.beginPath();
          ctx.fillStyle = 'lightblue';
          ctx.arc(0, 0, halfH , 0, 2 * Math.PI);
          ctx.fill();
          ctx.stroke();
          ctx.closePath();
          ctx.beginPath();
          ctx.fillStyle = 'blue';
          ctx.strokeStyle = 'black';
          ctx.moveTo(0, -halfH);
          ctx.lineTo(fourthH, halfH);
          ctx.lineTo(0, fourthH);
          ctx.lineTo(- fourthH, halfH);
          ctx.lineTo(0, - halfH);
          ctx.fill();
          ctx.stroke();
          ctx.closePath();
        }
        function resizeArea() {
          width = jDiv.width();
          height = jDiv.height();
          canvas.get(0).width = width;
          canvas.get(0).height = height;
          // console.log('w/h : ' + width + '/' + height);
          drawIndicator();
        }
        this.redraw = function () {
          resizeArea();
        };
        this.push = function (angle) {
          var newAngle = angle * Math.PI / 180;
          // ctx.rotate(currentAngle - newAngle);
          currentAngle = newAngle;
          // console.log('push ('+ angle + ')!');
          drawIndicator();
        };

        if(jDiv.height() < 50) {
          jDiv.height(50);
        }
        width = jDiv.width();
        height = jDiv.height();
        // console.log('w/h : ' + width + '/' + height);
        width = (width < height) ? height: width;
        canvas = $('<canvas>').width(jDiv.width(width).width()).height(jDiv.height()).appendTo(jDiv);
        ctx = canvas.get(0).getContext('2d');
        jDiv.resize(function () { resizeArea(); } );
        // if(jDiv.height() < 50) {
        //   jDiv.height(50);
        // } else {
        // jDiv.resize();
        // }
        this.redraw();
      };
  exports.OrientationMonitor = function () {
    var _orientation = new exports.Orientation(),
        _rotation = new exports.Orientation(),
        _isLogging = false,
        _triggerDelta = 0,
        _precision = 4;
    //init part:

    //end of initialization part
    function onOrientationChange(event) {
      var orientationEvent = event.originalEvent, newPitch, newRoll, newYaw, newCompass;
      newPitch = orientationEvent.beta.toFixed(_precision);
      newRoll = orientationEvent.gamma.toFixed(_precision);
      newYaw = orientationEvent.alpha.toFixed(_precision);
      newCompass = orientationEvent.webkitCompassHeading.toFixed(_precision);
      if (_triggerDelta <= 0
          || noticableChange(_orientation.pitch, newPitch)
          || noticableChange(_orientation.roll, newRoll)
          || noticableChange(_orientation.yaw, newYaw)
          || noticableChange(_orientation.compass, newCompass)){
            // console.log('changed noticed!');
            _orientation.update(newPitch, newRoll, newYaw, newCompass);
            // console.log('orientation updated');
            $(window).trigger('orientationChangedEvent', [_orientation.getData()]);
      } //else we ignore this change
    }
    function onDeviceMotion(event) {
      var rotationRate = event.originalEvent.rotationRate, newPitch, newRoll, newYaw, newCompass;
      newPitch = rotationRate.beta.toFixed(_precision);
      newRoll = rotationRate.gamma.toFixed(_precision);
      newYaw = rotationRate.alpha.toFixed(_precision);

      if (_triggerDelta <= 0
          || noticableChange(_rotation.pitch, newPitch)
          || noticableChange(_rotation.roll, newRoll)
          || noticableChange(_rotation.yaw, newYaw)){
            // console.log('changed noticed!');
            _rotation.update(newPitch, newRoll, newYaw, newCompass);
            // console.log('orientation updated');
            $(window).trigger('rotationRateChangedEvent', [_rotation.getData()]);
      } //else we ignore this change
    }
    function noticableChange(oldVal, newVal) {
      var delta =  Math.abs(oldVal - newVal);
      return delta > _triggerDelta;
    }
    this.start_logging = function (newDelta, newPrecision) {
      if (_isLogging) {
        console.log('the Monitor is already running... ');
        return;// allready running...
      }
      if(typeof newDelta === 'number') {
        _triggerDelta = newDelta;
      }
      if(typeof newPrecision === 'number') {
        _precision = newPrecision;
      }
      console.log('starting to log with threshold '+_triggerDelta);
      _isLogging = true;
      $(window).on('deviceorientation', onOrientationChange);
      $(window).on('devicemotion', onDeviceMotion);
    };
    this.stop_logging = function () {
      $(window).off('deviceorientation', onOrientationChange);
      $(window).off('devicemotion', onDeviceMotion);
      _isLogging = false;
    };
    this.__defineGetter__('triggerDelta', function () {return _triggerDelta;});
    this.__defineSetter__('triggerDelta', function (val) {
      if (typeof val === 'number') _triggerDelta = val;
    });
    this.__defineGetter__('precision', function () {return _precision;});
    this.__defineSetter__('precision', function (val) {
      if (typeof val === 'number') _precision = val;
    });

  };
}) (this.leojpod.orientation = {});

