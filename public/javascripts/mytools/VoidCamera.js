/*global THREE*/

THREE.CombinedCamera = function (camera1, camera2) {
  THREE.Camera.call(this);
  this.firstCam = camera1;
  this.secondCam = camera2;
};

THREE.CombinedCamera.prototype = Object.create(THREE.PerspectiveCamera.prototype);

THREE.CombinedCamera.prototype.setLens = function (focalLenght, frameHeight) {
  this.firstCam.setLens(focalLenght, frameHeight);
  this.secondCam.setLens(focalLenght, frameHeight);
};

THREE.CombinedCamera.prototype.setViewOffset = function ( fullWidth, fullHeight, x, y, width, height ) {
  this.firstCam.setViewOffset(fullWidth, fullHeight, x, y, width, height);
  this.secondCam.setViewOffset(fullWidth, fullHeight, x, y, width, height);
};

THREE.CombinedCamera.prototype.updateProjectionMatrix = function () {
  this.firstCam.updateProjectionMatrix();
  this.secondCam.updateProjectionMatrix();
};

THREE.CombinedCamera.prototype.lookAt = function (vector) {
  this.firstCam.lookAt(vector);
  this.secondCam.lookAt(vector);
};