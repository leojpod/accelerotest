$(document).ready(function () {
  var c = window.console;
  window.console = {
    toPanel: function (message) {
      // console.log('toPanel');
      $('#console-panel-content').append(message + '<br/>');
      $('#console-panel').trigger( "updatelayout" );
    },
    debug: function (message) {
      c.error("(DEBUG)#" + message);
    },
    verbose: function (message) {
      c.error("(VERBOSE)#" + message);
    },
    error: function (message) {
      this.toPanel('err: ' + message);
      c.error(message);
    },
    warn: function (message) {
      c.warn(message);
    },
    log: function (message) {
      this.toPanel(message);
      c.log(message);
    },
    info: function (message) {
      c.info(message);
    }
  };
  $('#clear-console').click(function () {
    console.log('clearing the mess of logs');
    $('#console-panel-content').html('');
    $('#console-panel').trigger( "updatelayout" );
  });
});
