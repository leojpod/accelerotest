/*global $, leojpod:true, THREE, requestAnimationFrame*/

'use strict';
if (typeof THREE === 'undefined') {
  throw 'three.js is requried for this module!!!';
}
if (typeof this.leojpod === 'undefined') {
  this.leojpod = {};
}
(function (exports) {
  'use strict';
  exports.World3D = function(container, useCSS3D) {
    var //canvasCamera, cssCamera, combinedCamera,
      mainCamera, loader,
      canvasScene, cssScene, canvasRenderer, cssRenderer, map, plane,
      height, width, isAnimating = false;
    function resizeArea () {
      height = $(container).height();
      width = $(container).width();
      console.log('resizing the world... ' + width + '/' + height);
      canvasRenderer.setSize(width, height);
      // $(renderer.domElement).height(height).width(width);
      if (useCSS3D === true) {
        cssRenderer.setSize(width, height);
        // $(cssRenderer.domElement).height(height).width(width);
      }
      // combinedCamera.setAspect(width/height);
      // combinedCamera.updateProjectionMatrix();
      mainCamera.aspect = (width/height);
      mainCamera.updateProjectionMatrix();
    }
    function animate() {
      if(isAnimating === true){
        requestAnimationFrame(animate);
        render();
      }
    }
    function render() {
      // console.log('rendering');
      canvasRenderer.render(canvasScene, mainCamera);
      if (useCSS3D === true) {
        cssRenderer.render(cssScene, mainCamera);
      }
    }
    function placeCamera(vector, target) {
      mainCamera.position = vector;
      if(typeof target !== 'undefined') {
        mainCamera.lookAt(target);
      }
    }
    function rotateCamera(vector) {
      mainCamera.rotation = (vector);
    }
    function initPlane() {
      //this shoudl work but doesn't...
      // loader = new THREE.JSONLoader();
      // console.log('JSON loader created');
      // loader.load('/public/models/plane.json', function (geometry, materials) {
      //   console.log('model fetched');
      //   //geometry.computeVertexNormals();
      //   plane = new THREE.Mesh( geometry, materials);
      //   canvasScene.add(plane);
      //   plane.position.z = 100;
      //   console.log('model added and placed');
      // });

      //so let's do it by hand...
      // plane = new THREE.Object3D();

      //or let's try to use the sketchup imports..
      var loader = new THREE.ColladaLoader();
      loader.load('public/models/colorPlane.dae', function (result) {
        plane = result.scene.children[0];
        plane.setMaterial(
          new THREE.MeshBasicMaterial({color: 0xaa00aa})
        );
        plane.position = new THREE.Vector3(0, 0, 20);
        console.log(plane);
        plane.scale = new THREE.Vector3(0.5,0.5,0.5);
        canvasScene.add(plane);
      });
    }
    function init() {
      var resizeTimer, mapNode = $(container).children('img');
      console.log('word3D.init -> ' + mapNode.get(0));
      // canvasCamera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
      mainCamera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
      canvasScene = new THREE.Scene();
      canvasRenderer = new THREE.CanvasRenderer();

      if(useCSS3D === true) {
        // cssCamera = new THREE.PerspectiveCamera(45, 1, 1, 10000);
        // mainCamera = new THREE.CombinedCamera(canvasCamera, cssCamera);
        cssScene = new THREE.Scene();
        //init the map here
        map = new THREE.CSS3DObject ( $('<div>').append(mapNode).get(0) );

        cssScene.add(map);
        cssRenderer = new THREE.CSS3DRenderer();
        cssRenderer.setSize(100, 100);
        $(container).append(cssRenderer.domElement);
        canvasRenderer.setSize(100, 100);
        canvasRenderer.domElement.style.position = 'absolute';
        canvasRenderer.domElement.style.top = 0; //this is not right! and should be changed!
        // $(container).append(renderer.domElement);
      } else {
        //init the map here
        var mapPlane, mapHeight, mapWidth,
          mapMaterial, mapTexture;
        // mainCamera = new THREE.combinedCamera(canvasCamera);
        mapWidth = mapNode.width();
        mapHeight = mapNode.height();
        console.log('about to create a planeGeometry of ' + mapWidth + 'x' + mapHeight);
        mapPlane = new THREE.PlaneGeometry(mapWidth, mapHeight, 10, 10);
        console.log(' image src = ' + mapNode.attr('src'));
        mapTexture = new THREE.ImageUtils.loadTexture(mapNode.attr('src'));
        mapTexture.wrapS = THREE.RepeatWrapping;
        mapTexture.wrapT = THREE.RepeatWrapping;
        mapTexture.repeat.set(1,1);
        console.log(' creating a material');
        mapMaterial = new THREE.MeshBasicMaterial({
          map: mapTexture,
          // overdraw: false
          overdraw: true
        });
        console.log('creating a mesh');
        map = new THREE.Mesh(mapPlane, mapMaterial);
        map.scale.x = 1;
        map.scale.y = 1;
        map.scale.z = 1;
        canvasScene.add(map);
        console.log('mesh added!');
        canvasRenderer.setSize(100,100);
        mapNode.remove();
      }
      console.log('let\'s load the plane!');
      initPlane();
      $(window).resize(function() {
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(resizeArea, 100);
      });
      $(container).append(canvasRenderer.domElement);
      // map.rotation = new THREE.Vector3(Math.PI / 4, Math.PI / 4, 0 ); // for debug purpose
      placeCamera( new THREE.Vector3(0,0, 500)); //This distance should actually be a function of the map size!
      // map.position.z = -20;
      // map.position = new THREE.Vector3(0,0,-20);
      resizeArea();
      render();
    }
    this.setAnimate = function (bool) {
      if (bool === false) {
        isAnimating = false;
      } else {
        if(isAnimating === false){
          isAnimating = true;
          animate();
        } /*else { // well else it already is true...
          isAnimating = true;
        }*/
      }
    };
    this.setRotation = function (rotationVector) {
      rotateCamera(rotationVector);
    };
    this.setPlacement = function (positionVector, target) {
      placeCamera(positionVector, target);
    };
    this.getCamera = function () {
      return mainCamera;
    };
    this.getElement = function () {
      return canvasRenderer.domElement;
    };
    this.movePlane = function (newPositionVector, target) {
      var oldPosition = plane.position.clone(), delta, newRotation;
      plane.position = newPositionVector;
      //now let's compute the correct rotation:
      delta = newPositionVector.clone().sub(oldPosition);
      console.log(delta);
      if (typeof target === 'undefined'){
        newRotation = new THREE.Vector3(Math.atan(delta.z/(Math.sqrt(delta.x^2 + delta.y^2))), 0, Math.atan(delta.y/delta.x));
        console.log(newRotation);
        plane.rotation = newRotation;
      }
    };
    //TODO add limits to the rotation and the panning

    init();
    console.log('world init is completed!');
  };
}) (this.leojpod.world = {});
console.log('3DWord loaded!')