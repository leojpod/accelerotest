$(document).ready(function () {
  $(window).resize(function () {
    var height = $('body').height(),
      content = $('#content-wrapper');
    height -= $('[data-role="header"]').outerHeight();
    height -= $('[data-role="footer"]').outerHeight();

    content.height(height);
  });
  $(window).resize();
});
