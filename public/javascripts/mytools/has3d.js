/*global $, leojpod:true, THREE, requestAnimationFrame*/

'use strict';
if (typeof this.leojpod === 'undefined') {
  this.leojpod = {};
}
(function (exports) {
  exports.has3D = function () {
    var el = $('<p>'), t, has3d,
    transforms = {
        'WebkitTransform':'-webkit-transform',
        'OTransform':'-o-transform',
        'MSTransform':'-ms-transform',
        'MozTransform':'-moz-transform',
        'transform':'transform'
    };

    /* Add it to the body to get the computed style.*/
    el.appendTo($('body'));

    for(t in transforms){
        if( el.get(0).style[t] !== undefined ){
            el.get(0).style[t] = 'matrix3d(1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1)';
            has3d = window.getComputedStyle(el.get(0)).getPropertyValue( transforms[t] );
        }
    }

    if( has3d !== undefined ){
        return has3d !== 'none';
    } else {
        return false;
    }
  };
}) (this.leojpod.threeDTools = {});