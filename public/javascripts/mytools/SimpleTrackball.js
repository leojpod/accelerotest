/*global THREE*/
/**
 * @author leo Jpod
 *
 */

THREE.SimpleTrackballControls = function (camera, domElement) {
  'use strict';
  var self = this, currentMode, zoom,
    // camera,
    originalCameraValues, element;

  // this.world = world;
  // camera = world.getCamera();
  // camera = world.getCamera();
  // console.log(camera);
  originalCameraValues = {
    position: camera.position.clone(),
    rotation: camera.rotation.clone(),
    // more will come after if needed!
  };
  zoom = {
    current: 5,
    max: 10,
    min: 1,
  };
  element = (domElement !== undefined) ? domElement : document;
  this.MODE = { LOOK: 0, MOVE: 1 };
  this.zoomFactor = 0.7;
  // methods
  function onResize() {

  }

  function onPanStart() {

  }
  function onPanStop() {

  }
  function zoomIn() {
    zoom.current += 1;
    if (zoom.current > zoom.max){
      zoom.current = zoom.max;
    } else {
      camera.position.multiplyScalar(self.zoomFactor);
    }
  }
  function zoomOut() {
    zoom.current -= 1;
    if (zoom.current < zoom.min){
      zoom.current = zoom.min;
    } else {
      camera.position.multiplyScalar(1/self.zoomFactor);
    }
  }
  function onRotateStart() {

  }
  function onRotateStop() {

  }

  // API

  this.setMode = function (mode) {
    if(mode === this.MODE.LOOK || mode === this.MODE.MOVE){
      currentMode = mode;
    }
  };
  this.stepZoom = function (inOrOut) {
    if(inOrOut === true) {
      zoomIn();
    } else if(inOrOut === false) {
      zoomOut();
    }
  };
};

THREE.SimpleTrackballControls.prototype = Object.create(THREE.EventDispatcher.prototype);
