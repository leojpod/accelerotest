(function () {
  //putting everything within  a closure to avoid messing up with anything
  var START_LOGGING = 'Start logging orientation and compass',
      STOP_LOGGING = 'Stop logging them now',
      lastOrientation = {count : 0},
      lastOrientationEvent = {},
      orientationInterval,
      ValueLogger = function (makeUpValues) {
        var _val = 0, _min = 1000, _max = -1000;
        if (typeof makeUpValues === 'undefined') {
          makeUpValues = false;
        }
        this.min = function () {
          return _min;
        };
        this.max = function () {
          return _max;
        };
        this.getValue = function () {
          return _val;
        };
        this.push = function (val) {
          if(typeof val === 'undefined' || val === null) {
            _val = (makeUpValues) ? (Math.random() * 360 - 180): 0;
          } else {
            _val = val;
          }
          _min = (_val < _min) ? Math.floor(_val) : _min;
          _max = (_val > _max) ? Math.round(_val) : _max;
        };
      },
      SimpleAngleVisualisor = function (div) {
        var canvas, ctx, jDiv = $(div), currentAngle, width, height;

        function drawIndicator() {
          var halfH, fourthH;
          width = jDiv.width();
          height = jDiv.height();
          halfH = Math.floor(height/2);
          fourthH = Math.floor(height/4);
          ctx.setTransform(1, 0, 0, 1, 0, 0);
          ctx.clearRect(0, 0, width, height);
          ctx.translate(width / 2, halfH);
          ctx.rotate(currentAngle);
          ctx.beginPath();
          ctx.lineWidth = 4;
          ctx.lineCap = 'round';
          ctx.lineJoin = 'round';
          ctx.fillStyle = 'blue';
          ctx.strokStyle = 'black';
          ctx.shadowOffsetX = 5;
          ctx.shadowOffsetY = 5;
          ctx.shadowBlur = 5;
          ctx.moveTo(0, -halfH);
          ctx.lineTo(fourthH, halfH);
          ctx.lineTo(0, fourthH);
          ctx.lineTo(- fourthH, halfH);
          ctx.lineTo(0, - halfH);
          ctx.fill();
          ctx.closePath();
        }
        function resizeArea() {
          var halfH, fourthH;
          width = jDiv.width();
          height = jDiv.height();
          canvas.get(0).width = width;
          canvas.get(0).height = height;
          // console.log('w/h : ' + width + '/' + height);
          drawIndicator();
        }
        this.redraw = function () {
          resizeArea();
        };
        this.push = function (angle) {
          var newAngle = angle * Math.PI / 180;
          // ctx.rotate(currentAngle - newAngle);
          currentAngle = newAngle;
          drawIndicator();
        };

        if(jDiv.height() < 50) {
          jDiv.height(50);
        }
        canvas = $('<canvas>').width(jDiv.width()).height(jDiv.height()).appendTo(jDiv);
        ctx = canvas.get(0).getContext('2d');
        jDiv.resize(function () { resizeArea(); } );
        // if(jDiv.height() < 50) {
        //   jDiv.height(50);
        // } else {
        jDiv.resize();
        // }
        // this.redraw();
      },
      alphaVis, betaVis, gammaVis, compassVis;


  function start_logging() {
    $('#start_stop_log').text(STOP_LOGGING).unbind('click').click(stop_logging);
    //regularly print the last event received
    orientationInterval = setInterval(function () { printOrientation(lastOrientationEvent, lastOrientation);}, 250);
    //add the actuall listener
    // window.addEventListener('deviceorientation', onDeviceOrientationChange, false);
    $( window ).on('deviceorientation', orientationChange);
    // window.addEventListener('deviceorientation', orientationChange, false);
  }
  function orientationChange(event) {
    lastOrientationEvent = event.originalEvent;
    lastOrientation.count += 1;
    if (lastOrientation.count % 10 === 0) {
      console.log('#' + lastOrientation.count + " and counting...");
    }
    lastOrientation.alpha.push(lastOrientationEvent.alpha);
    lastOrientation.beta.push(lastOrientationEvent.beta);
    lastOrientation.gamma.push(lastOrientationEvent.gamma);
    lastOrientation.compass.push(lastOrientationEvent.webkitCompassHeading);
  }
  function printOrientation (orientationEvent, orientation) {
    var a, b, g, c;
    console.log('printOrientation');
    a = orientation.alpha.getValue();
    b = orientation.beta.getValue();
    g = orientation.gamma.getValue();
    c = orientation.compass.getValue();
    console.log('a/b/g/c --> ' + a + '/'+ b + '/'+ g + '/'+ c);
    console.log('window orientation: ' + window.orientation);
    // console.log(orientationEvent);
    // console.log(orientation);
    $('#orientation').text(
      '#'+ orientation.count +' :  '
      + a.toFixed(2) + ' / '
      + b.toFixed(2) + ' / '
      + g.toFixed(2) + ' / ' + c.toFixed(2));
    alphaVis.push(a);
    betaVis.push(b);
    gammaVis.push(g);
    compassVis.push(c);
    $('#alpha-min').val(orientation.alpha.min());
    $('#alpha-max').val(orientation.alpha.max());
    $('#beta-min').val(orientation.beta.min());
    $('#beta-max').val(orientation.beta.max());
    $('#gamma-min').val(orientation.gamma.min());
    $('#gamma-max').val(orientation.gamma.max());
    $('#compass-min').val(orientation.compass.min());
    $('#compass-max').val(orientation.compass.max());
    // $('div[data-role="rangeslider"]').rangeslider( "enable" );
    $('div[data-role="rangeslider"]').rangeslider( "refresh" );
  }
  function stop_logging() {
    $('#start_stop_log').val(START_LOGGING).unbind('click').click(start_logging);
    clearInterval(orientationInterval);
    $( window ).off('deviceorientation', orientationChange);
  }
  function init() {
    console.log('init!');
    $('#start_stop_log').val(START_LOGGING).click(start_logging);
    alphaVis = new SimpleAngleVisualisor(jQuery('#alpha'));
    betaVis = new SimpleAngleVisualisor(jQuery('#beta'));
    gammaVis = new SimpleAngleVisualisor(jQuery('#gamma'));
    compassVis = new SimpleAngleVisualisor(jQuery('#compass'));
    lastOrientation.alpha = new ValueLogger(true);
    lastOrientation.beta = new ValueLogger(true);
    lastOrientation.gamma = new ValueLogger(true);
    lastOrientation.compass = new ValueLogger(true);
  }


  $(document).ready(init);
}) ();