
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index', {page_id: 'index', page_title: 'POC - 3D Trip viewer'});
};

exports.box = function(req, res){
  res.render('box', {page_id: 'box', page_title: '3D box view'});
};

exports.test = function(req, res) {
  res.render('acceleroTest', {page_id: 'accelero testing', page_title: 'accelero testing'});
};

exports.map = function(req, res) {
  res.render('map', {page_id: 'map-test', page_title: '3d Map viewer'});
};
